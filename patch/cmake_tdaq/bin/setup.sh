# Usage: source setup.sh [config]
#
# where 'config' is using the standard syntax: ARCH-OS-COMPILER-BUILD
# e.g. x86_64-centos7-gcc8-opt or x86_64-centos7-gcc11-opt
#

############## configuration variables ################
# You can override this in your environment for testing other versions.

export REGMAP_VERSION=${REGMAP_VERSION:=0x0400}
case "${REGMAP_VERSION}" in
    0x0400)
        export REGMAP_FW=rm4
        ;;
    *)
        export REGMAP_FW=rm5
        ;;
esac

# Latest default if release not defined otherwise.
#
# If we have sourced the run-time setup via cm_setup.sh, the
# TDAQ_LCG_RELEASE defines what we use.
lcg=${TDAQ_LCG_RELEASE:-LCG_101}
CMAKE_VERSION=${CMAKE_VERSION:=3.23.1}
export LCG_VERSION=101
export LCG_VERSION_POSTFIX=
export LCG_FULL_VERSION=${LCG_VERSION}${LCG_VERSION_POSTFIX}
export LCG_QT_VERSION=${LCG_FULL_VERSION}
export QT5_VERSION=5.12.4
export LIBXKBCOMMON_VERSION=0.7.1
export PYTHON_VERSION=3.9.6

export TDAQ_BASE=$(dirname $(dirname $(dirname $(readlink -f ${BASH_SOURCE[0]:-${(%):-%x}}))))

# This can still be overriden on the command line...
case "$1" in
    --lcg=*)
        lcg=${1#--lcg=}
        shift
        ;;
    *)
        ;;
esac

# Determine <arch>-<os>, used to find various paths for basic binutils and compilers
# without the need for the <compiler>-<opt|dbg> part of the tag.
export TDAQ_HOST_ARCH=${TDAQ_HOST_ARCH:=$(uname -i)-$(/bin/sh -c '. /etc/os-release; echo ${ID}${VERSION_ID}' | tr -d \. | sed -e 's;rocky;centos;' -e 's;rhel;centos;' -e 's;almalinux;centos;' -e 's;ol;centos;' )}


############## configuration variables ################
# You can override this in your environment for testing other versions.

# legacy standard AFS location of LCG software
# DEFAULT_LCG_BASE="/afs/cern.ch/sw/lcg/releases"
# favour new new standard location if CVMF available
# [ -d /cvmfs/sft.cern.ch/lcg/releases ] && DEFAULT_LCG_BASE="/cvmfs/sft.cern.ch/lcg/releases"

# case "${lcg}" in
#     dev*)
#        DEFAULT_LCG_BASE="/cvmfs/sft-nightlies.cern.ch/lcg/nightlies"
#        ;;
# esac

# you can still override this defining LCG_RELEASE_BASE beforehand
# export LCG_RELEASE_BASE=${LCG_RELEASE_BASE:=${DEFAULT_LCG_BASE}}
# export LCG_BASE=$(dirname ${LCG_RELEASE_BASE})
# export LCG_BASE=${LCG_BASE:=/cvmfs/sft.cern.ch/lcg}
echo "LCG_BASE=${LCG_BASE}"

# The location of LCG contrib and external
#EXTERNAL_BASE=${EXTERNAL_BASE:=${LCG_BASE}/external}
#CONTRIB_BASE=${CONTRIB_BASE:=${LCG_BASE}/contrib}
#TOOL_BASE=${TOOL_BASE:=/cvmfs/atlas.cern.ch/repo/sw/tdaq/tools/${TDAQ_HOST_ARCH}}

# The location and version of CMake to use
echo "CMAKE_BASE=${CMAKE_BASE}"
echo "CMAKE_PATH=${CMAKE_PATH}"
#CMAKE_BASE=${CMAKE_BASE:=${TOOL_BASE}/CMake}
#CMAKE_PATH=${CMAKE_PATH:=${CMAKE_BASE}/${CMAKE_VERSION}/bin}

# For TDAQ projects
#CMAKE_PROJECT_PATH=${CMAKE_PROJECT_PATH:=/cvmfs/atlas.cern.ch/repo/sw/tdaq}

# export JAVA_HOME=${JAVA_HOME:=${EXTERNAL_BASE}/Java/JDK/1.8.0/amd64}

############## end of configuration variables ################
if [ -z "${1}" -o  "${1}" == "--" ]
then
    # no binary tag
    UNAME=`uname -r`
    case "${UNAME}" in
        *)
            export BINARY_TAG="x86_64-centos7-gcc11-opt"
            ;;
    esac
else
    export BINARY_TAG=${1}
    shift
fi

echo "Setting up FELIX (developer)"
echo "- REGMAP: ${REGMAP_VERSION}"
echo "- TDAQ_HOST_ARCH: ${TDAQ_HOST_ARCH}"
echo "- BINARY_TAG: ${BINARY_TAG}"
echo "- LCG_FULL_VERSION: ${LCG_FULL_VERSION}"

# if [ ! -d "${LCG_BASE}" ]; then
#     echo "LCG_BASE Directory Not Found: ${LCG_BASE}"
#     return 1
# fi


# Setup path for CMAKE
export PATH=$(dirname $(readlink -f ${BASH_SOURCE[0]:-${(%):-%x}})):${CMAKE_PATH}:${PATH}

# Choose compiler, this has to reflect what you choose in the tag to
# cmake_config later
_conf=${BINARY_TAG}

gcc_requested=$(echo "${_conf}" | cut -d- -f3 | tr -d 'gcc')
gcc_version=$(g++ --version | head -1 | awk '{ print $NF; }' | cut -d. -f1)
if [ "${gcc_version}" -ne $(echo "${gcc_requested}" | cut -d. -f1) ]; then
   echo "warning: gcc version ${gcc_requested} needed, but version ${gcc_version} found" 1>&2
fi


# case "${_conf}" in
#     *-gcc*)
#         # if [ -n "${lcg}" ] && [ -f "${LCG_RELEASE_BASE}/${lcg}/LCG_externals_${_conf}.txt" ]; then
#             # gcc_requested=$(grep '^COMPILER:' ${LCG_RELEASE_BASE}/${lcg}/LCG_externals_${_conf}.txt | cut -d' ' -f2 | cut -d';' -f2)
#         # else
#             gcc_requested=$(echo "${_conf}" | cut -d- -f3 | tr -d 'gcc')
#         # fi
#         # if [ -f ${CONTRIB_BASE}/gcc/${gcc_requested}binutils/${TDAQ_HOST_ARCH}/setup.sh ]; then
#         #     source ${CONTRIB_BASE}/gcc/${gcc_requested}binutils/${TDAQ_HOST_ARCH}/setup.sh
#         # elif [ -f ${CONTRIB_BASE}/gcc/${gcc_requested}/${TDAQ_HOST_ARCH}/setup.sh ]; then
#         #     source ${CONTRIB_BASE}/gcc/${gcc_requested}/${TDAQ_HOST_ARCH}/setup.sh
#         # else
#             gcc_version=$(g++ --version | head -1 | awk '{ print $NF; }' | cut -d. -f1)
#             if [ "${gcc_version}" -ne $(echo "${gcc_requested}" | cut -d. -f1) ]; then
#                echo "warning: gcc version ${gcc_requested} needed, but version ${gcc_version} found" 1>&2
#             fi
#         # fi
#         unset gcc_requested gcc_version
#         ;;
#     *-clang*)
#         if [ -n "${lcg}" ] && [ -f ${LCG_RELEASE_BASE}/${lcg}/LCG_externals_${_conf}.txt ]; then
#             clang_requested=$(grep '^COMPILER:' ${LCG_RELEASE_BASE}/${lcg}/LCG_externals_${_conf}.txt | cut -d' ' -f2 | cut -d';' -f2)
#         else
#             clang_requested=$(echo "${_conf}" | cut -d- -f3 | tr -d 'clang')
#         fi
#         source ${CONTRIB_BASE}/clang/${clang_requested}/${TDAQ_HOST_ARCH}/setup.sh
#         unset clang_requested
#         ;;
#     *)
#         #default
#         source ${CONTRIB_BASE}/gcc/8binutils/${TDAQ_HOST_ARCH}/setup.sh
#         ;;
# esac

#export gcc_config_version

# For FELIX
#export PATH=${LCG_BASE}/releases/LCG_${LCG_FULL_VERSION}/Python/${PYTHON_VERSION}/${BINARY_TAG}/bin:${PATH}

export SOURCE_PATH=$(dirname $(dirname $(dirname $(readlink -f ${BASH_SOURCE[0]}))))
export BINARY_TAG_PATH=${SOURCE_PATH}/${BINARY_TAG}
# export EXTERNAL_PATH=${SOURCE_PATH}/external

# Setup of PATH to find most binaries
export PATH=${BINARY_TAG_PATH}/felix-starter:${PATH}
export PATH=${BINARY_TAG_PATH}/felix-star:${PATH}
export PATH=${BINARY_TAG_PATH}/ftools:${PATH}
export PATH=${BINARY_TAG_PATH}/elinkconfig:${PATH}
export PATH=${BINARY_TAG_PATH}/flxcard:${PATH}
export PATH=${BINARY_TAG_PATH}/felixbus-client:${PATH}
export PATH=${BINARY_TAG_PATH}/fatcat:${PATH}

# Setup LD_LIBRARY_PATH
export LD_LIBRARY_PATH=${SOURCE_PATH}/drivers_rcc/lib64:${LD_LIBRARY_PATH}
export LD_LIBRARY_PATH=${BINARY_TAG_PATH}/drivers_rcc:${LD_LIBRARY_PATH}
export LD_LIBRARY_PATH=${BINARY_TAG_PATH}/felix-bus-fs:${LD_LIBRARY_PATH}
export LD_LIBRARY_PATH=${BINARY_TAG_PATH}/felix-client:${LD_LIBRARY_PATH}
export LD_LIBRARY_PATH=${BINARY_TAG_PATH}/felix-client-thread:${LD_LIBRARY_PATH}
export LD_LIBRARY_PATH=${BINARY_TAG_PATH}/felix-mapper:${LD_LIBRARY_PATH}
export LD_LIBRARY_PATH=${BINARY_TAG_PATH}/felixbase:${LD_LIBRARY_PATH}
export LD_LIBRARY_PATH=${BINARY_TAG_PATH}/felixbus:${LD_LIBRARY_PATH}
export LD_LIBRARY_PATH=${BINARY_TAG_PATH}/felixcore:${LD_LIBRARY_PATH}
export LD_LIBRARY_PATH=${BINARY_TAG_PATH}/felixpy:${LD_LIBRARY_PATH}
export LD_LIBRARY_PATH=${BINARY_TAG_PATH}/flxcard:${LD_LIBRARY_PATH}
export LD_LIBRARY_PATH=${BINARY_TAG_PATH}/ftools:${LD_LIBRARY_PATH}
export LD_LIBRARY_PATH=${BINARY_TAG_PATH}/hdlc_coder:${LD_LIBRARY_PATH}
export LD_LIBRARY_PATH=${BINARY_TAG_PATH}/netio:${LD_LIBRARY_PATH}
export LD_LIBRARY_PATH=${BINARY_TAG_PATH}/netio-next:${LD_LIBRARY_PATH}
export LD_LIBRARY_PATH=${BINARY_TAG_PATH}/packetformat:${LD_LIBRARY_PATH}
export LD_LIBRARY_PATH=${BINARY_TAG_PATH}/regmap:${LD_LIBRARY_PATH}
export LD_LIBRARY_PATH=${BINARY_TAG_PATH}/tdaq_tools:${LD_LIBRARY_PATH}

# export LD_LIBRARY_PATH=${EXTERNAL_PATH}/czmq/default/${BINARY_TAG}/lib:${LD_LIBRARY_PATH}
# export LD_LIBRARY_PATH=${EXTERNAL_PATH}/docopt/default/${BINARY_TAG}/lib:${LD_LIBRARY_PATH}
# export LD_LIBRARY_PATH=${EXTERNAL_PATH}/jwrite/default/${BINARY_TAG}/lib:${LD_LIBRARY_PATH}
# export LD_LIBRARY_PATH=${EXTERNAL_PATH}/libfabric/default/${BINARY_TAG}/lib:${LD_LIBRARY_PATH}
# export LD_LIBRARY_PATH=${EXTERNAL_PATH}/libnuma/default/${BINARY_TAG}/lib:${LD_LIBRARY_PATH}
# export LD_LIBRARY_PATH=${EXTERNAL_PATH}/logrxi/default/${BINARY_TAG}/lib:${LD_LIBRARY_PATH}
# export LD_LIBRARY_PATH=${EXTERNAL_PATH}/simdjson/default/${BINARY_TAG}/lib:${LD_LIBRARY_PATH}
# export LD_LIBRARY_PATH=${EXTERNAL_PATH}/yaml-cpp/default/${BINARY_TAG}/lib:${LD_LIBRARY_PATH}
# export LD_LIBRARY_PATH=${EXTERNAL_PATH}/zyre/default/${BINARY_TAG}/lib:${LD_LIBRARY_PATH}

# setup for felix python tools
export PYTHONPATH=${BINARY_TAG_PATH}:${PYTHONPATH}
export PYTHONPATH=${BINARY_TAG_PATH}/felix-def:${PYTHONPATH}
export PYTHONPATH=${BINARY_TAG_PATH}/felix-unit-test:${PYTHONPATH}
export PYTHONPATH=${BINARY_TAG_PATH}/felix-star:${PYTHONPATH}
export PYTHONPATH=${BINARY_TAG_PATH}/felix-star/tests/bus:${PYTHONPATH}
export PYTHONPATH=${BINARY_TAG_PATH}/felix-star/tests/config:${PYTHONPATH}
export PYTHONPATH=${BINARY_TAG_PATH}/felix-star/tests/elink:${PYTHONPATH}
export PYTHONPATH=${BINARY_TAG_PATH}/felix-star/tests/fid:${PYTHONPATH}
export PYTHONPATH=${BINARY_TAG_PATH}/felix-star/tests/ip:${PYTHONPATH}
export PYTHONPATH=${BINARY_TAG_PATH}/felix-star/tests/mode:${PYTHONPATH}
export PYTHONPATH=${BINARY_TAG_PATH}/felix-client:${PYTHONPATH}
export PYTHONPATH=${BINARY_TAG_PATH}/felix-config:${PYTHONPATH}
export PYTHONPATH=${BINARY_TAG_PATH}/felixbus:${PYTHONPATH}
export PYTHONPATH=${BINARY_TAG_PATH}/felix-bus-fs:${PYTHONPATH}
export PYTHONPATH=${BINARY_TAG_PATH}/flxcard_py:${PYTHONPATH}
export PYTHONPATH=${BINARY_TAG_PATH}/hdlc_coder:${PYTHONPATH}
export PYTHONPATH=${BINARY_TAG_PATH}/netio-next:${PYTHONPATH}
export PYTHONPATH=${BINARY_TAG_PATH}/data_transfer_tools:${PYTHONPATH}
export PYTHONPATH=${BINARY_TAG_PATH}/felix-tag:${PYTHONPATH}

# fix from FLX-272, QT setup, no keyboard activity
export QT_XKB_CONFIG_ROOT=/usr/share/X11/xkb
#export QT5_DIR=${LCG_BASE}/releases/LCG_${LCG_QT_VERSION}/qt5/${QT5_VERSION}/${BINARY_TAG}
export QT_QPA_PLATFORM_PLUGIN_PATH=${QT5_DIR}/plugins/platforms
export LD_LIBRARY_PATH=${QT5_DIR}/lib:${LD_LIBRARY_PATH}
#export LD_LIBRARY_PATH=${LCG_BASE}/releases/LCG_${LCG_QT_VERSION}/libxkbcommon/${LIBXKBCOMMON_VERSION}/${BINARY_TAG}/lib:${LD_LIBRARY_PATH}


# Setup CMAKE_PREFIX_PATH to find LCG
export CMAKE_PREFIX_PATH="${CMAKE_PROJECT_PATH} $(dirname $(dirname $(readlink -f ${BASH_SOURCE[0]:-${(%):-%x}})))/cmake"

#unset CMAKE_BASE CMAKE_VERSION CMAKE_PATH GCC_BASE EXTERNAL_BASE CONTRIB_BASE TOOL_BASE _conf lcg
#unset SOURCE_PATH BINARY_TAG_PATH EXTERNAL_PATH
unset CMAKE_BASE CMAKE_VERSION CMAKE_PATH GCC_BASE _conf lcg
unset SOURCE_PATH BINARY_TAG_PATH

echo "- PYTHON: `which python`"
