cmake_minimum_required(VERSION 3.4.3)

include(GNUInstallDirs)

set(CMAKE_CXX_STANDARD 17)
# set(CMAKE_CXX_FLAGS -DGIT_DESCRIBE="`cd ${CMAKE_CURRENT_SOURCE_DIR}; git describe --tags --always --dirty`")
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wall -O3 -g -pedantic")

if(NOT FELIX_INCLUDED)
  set(FELIX_INCLUDED TRUE)

  #
  # handle coverage
  #
  SET(FELIX_COVERAGE_LIBS "")
  SET(FELIX_COVERAGE_OPTS "")
  if(DEFINED FELIX_COVERAGE)
    MESSAGE( STATUS "Building with COVERAGE")
	  SET(FELIX_COVERAGE_LIBS "gcov")
    # optimize needs to be level 1 or more otherwise some inline functions end up to be unresolved links
	  SET(FELIX_COVERAGE_OPTS --coverage -O1 -DFELIX_COVERAGE)
  endif()

  #
  # find externals correctly
  #
  set(FELIX_EXTERNAL_DIR $ENV{FELIX_EXTERNAL_DIR}/include)

  #
  # extra functions
  #
  function(felix_add_external name version)
    MESSAGE( STATUS "    external " ${name} " - " ${version})
    #include_directories(${CMAKE_BINARY_DIR}/external/include/${name})
    #include_directories(${CMAKE_BINARY_DIR}/external/include)
    #link_directories(${CMAKE_BINARY_DIR}/external/${CMAKE_INSTALL_LIBDIR})
    #message("----  include: ${FELIX_EXTERNAL_DIR}/include/${name}")
    #message("      link   : ${CMAKE_BINARY_DIR}/external/${CMAKE_INSTALL_LIBDIR}")
    #if(${ARGC} EQUAL 3)
    #  set(bintag ${ARGV2})
    #  include_directories(${FELIX_EXTERNAL_DIR}/${name}/${version}/${bintag}/include)
    #  link_directories(${FELIX_EXTERNAL_DIR}/${name}/${version}/${bintag}/lib)
    #else()
    #  include_directories(${FELIX_EXTERNAL_DIR}/${name}/${version}/include)
    #endif()
  endfunction(felix_add_external)

  function(felix_library_version name version)
    MESSAGE( STATUS "    library " ${name} " - " ${version})
  endfunction(felix_library_version)

  #
  # Default settings
  #
  set(BUILD_SHARED_LIBS true)
  set(CMAKE_INCLUDE_CURRENT_DIR true)
  add_definitions(-D_GNU_SOURCE -D_REENTRANT -D__USE_XOPEN2K8 -DREGMAP_VERSION=$ENV{REGMAP_VERSION})
  set(BINTAG ${BUILDNAME})
  # set(LCG_RELEASE_BASE $ENV{LCG_BASE}/releases)
  # include_directories(${CMAKE_CURRENT_SOURCE_DIR}/cmake_tdaq/cmake/modules/include)

  include(FELIX-versions)

  #
  #
  #
  include (CTest)

  #
  # TDAQ
  #
  include(TDAQ)

  #
  # top-level project specific
  #
  # Use this variable to set the suffix version of LCG
  #set(LCG_VERSION_POSTFIX python3)
  set(LCG_VERSION_POSTFIX "")
  #tdaq_project(flx 3.4.0 USES LCG 84)
  if(NOLCG)
  tdaq_project(${PACKAGE} ${PACKAGE_VERSION} )
  else()
  tdaq_project(${PACKAGE} ${PACKAGE_VERSION} USES LCG $ENV{LCG_VERSION})
  endif()

  # Debug
  MESSAGE( STATUS "Binary Tag (BUILDNAME): " ${BUILDNAME})

endif()

#
# sub project specific
#
if(NOT ${PACKAGE} EQUAL "felix")
  tdaq_package(${PACKAGE_ARGS})
  project(${PACKAGE})
endif()
