# netio, netio-next
git clone https://gitlab.cern.ch/atlas-tdaq-felix/felix-tag.git
git clone https://gitlab.cern.ch/atlas-tdaq-felix/netio.git
git clone https://gitlab.cern.ch/atlas-tdaq-felix/netio-next.git


# felix-client
git clone https://gitlab.cern.ch/atlas-tdaq-felix/felix-def.git
git clone https://gitlab.cern.ch/atlas-tdaq-felix/felix-interface.git
git clone https://gitlab.cern.ch/atlas-tdaq-felix/felix-client-thread.git
git clone https://gitlab.cern.ch/atlas-tdaq-felix/felix-unit-test.git
git clone https://gitlab.cern.ch/atlas-tdaq-felix/felix-bus-fs.git
git clone https://gitlab.cern.ch/atlas-tdaq-felix/hdlc_coder.git
git clone https://gitlab.cern.ch/atlas-tdaq-felix/felix-client.git


# felix-star
git clone https://gitlab.cern.ch/atlas-tdaq-felix/flxcard.git
git clone https://gitlab.cern.ch/atlas-tdaq-felix/flxcard_py.git
git clone https://gitlab.cern.ch/atlas-tdaq-felix/drivers_rcc.git
git clone https://gitlab.cern.ch/atlas-tdaq-felix/regmap.git
git clone https://gitlab.cern.ch/atlas-tdaq-felix/felix-star.git


# felixcore
git clone https://gitlab.cern.ch/atlas-tdaq-felix/packetformat.git
git clone https://gitlab.cern.ch/atlas-tdaq-felix/felixbus.git
git clone https://gitlab.cern.ch/atlas-tdaq-felix/felixbase.git
git clone https://gitlab.cern.ch/atlas-tdaq-felix/felixcore.git


# misc
git clone https://gitlab.cern.ch/atlas-tdaq-felix/felig-tools.git
git clone https://gitlab.cern.ch/atlas-tdaq-felix/ftools.git
git clone https://gitlab.cern.ch/atlas-tdaq-felix/felix-mapper.git
git clone https://gitlab.cern.ch/atlas-tdaq-felix/felix-fpga-flash.git
git clone https://gitlab.cern.ch/atlas-tdaq-felix/firmware_loader.git
git clone https://gitlab.cern.ch/atlas-tdaq-felix/data_transfer_tools.git
git clone https://gitlab.cern.ch/atlas-tdaq-felix/wuppercodegen.git

