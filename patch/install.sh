#!/bin/bash

make install

cp -r ../felix-bus-fs/felixbus ../installed/include
cp -r ../netio-next/netio      ../installed/include

mkdir -p ../installed/include/felix

cp ../felix-interface/felix/felix_client_thread_interface.hpp  ../installed/include/felix/
cp ../felix-interface/felix/felix_client_thread_extension.hpp  ../installed/include/felix/

cp ../felix-client/felix/felix_client_context_handler.hpp      ../installed/include/felix/
cp ../felix-client/felix/felix_client_util.hpp                 ../installed/include/felix/
cp ../felix-client/felix/felix_client_info.hpp                 ../installed/include/felix/

cp ../felix-def/include/felix/felix_toflx.hpp                  ../installed/include/felix/

# lib
cp felix-client/libfelix-client-lib*  ../installed/lib/

