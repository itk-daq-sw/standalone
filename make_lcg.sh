#!/bin/bash

source scl_source enable devtoolset-11

export CC=/opt/rh/devtoolset-11/root/usr/bin/gcc
export CXX=/opt/rh/devtoolset-11/root/usr/bin/g++

export PATH=/opt/rh/devtoolset-11/root/usr/bin:${PATH}

# LCG_PATH
export BASE_DIR=$(dirname $(readlink -f ${BASH_SOURCE[0]:-${(%):-%x}}))
export LCG_BASE=${BASE_DIR}/lcg
echo "LCG_BASE = '${LCG_BASE}'"

export PATH=${LCG_BASE}/bin:${PATH}

export CPATH=${LCG_BASE}/include
export LD_LIBRARY_PATH=${LCG_BASE}/lib:${LD_LIBRARY_PATH}
export PKG_CONFIG_PATH=${LCG_BASE}/lib/pkgconfig:$PKG_CONFIG_PATH


function install_python() {
    echo "=== Installing Python-3.9.6 ==="
    cd ${DOWNLOADS}

    #sudo yum install gcc openssl-devel bzip2-devel libffi-devel zlib-devel

    wget -nv https://www.python.org/ftp/python/3.9.6/Python-3.9.6.tgz
    tar xzf Python-3.9.6.tgz
    cd Python-3.9.6
    ./configure --enable-optimizations --prefix=${LCG_BASE}
    make -j
    make install

    ln -s python3 ${LCG_BASE}/bin/python
    ln -s pip3 ${LCG_BASE}/bin/pip

    cd ${DOWNLOADS}
    rm -r Python-3.9.6
    rm Python-3.9.6.tgz

    # installing python packages
    python -m pip install pytest
}


function install_cmake() {
    echo "=== Installing CMake 3.25.1 ==="
    cd ${DOWNLOADS}

    wget -nv https://github.com/Kitware/CMake/releases/download/v3.25.1/cmake-3.25.1.tar.gz
    tar xzf cmake-3.25.1.tar.gz
    cd cmake-3.25.1
    ./bootstrap --prefix=${LCG_BASE}
    make -j
    make install

    cd ${DOWNLOADS}
    rm -f -r cmake-3.25.1
    rm -f cmake-3.25.1.tar.gz
}


function install_boost() {
    echo "=== Installing Boost 1.77.0 ==="
    cd ${DOWNLOADS}

    wget -nv https://boostorg.jfrog.io/artifactory/main/release/1.77.0/source/boost_1_77_0.tar.gz
    tar xzf boost_1_77_0.tar.gz
    cd boost_1_77_0
    ./bootstrap.sh --prefix=${LCG_BASE} --with-python=python
    ./b2 stage -j threading=multi link=shared
    ./b2 install threading=multi link=shared

    cd ${DOWNLOADS}
    rm -r boost_1_77_0
    rm boost_1_77_0.tar.gz
}

if [ -z "$1" ]; then
    echo "usage: make_lcg.sh [python | cmake | boost]"
    exit 1
fi

mkdir -p ${LCG_BASE}

export DOWNLOADS=${BASE_DIR}/downloads
mkdir -p ${DOWNLOADS}

if [[ $1 == "python" ]]; then
    install_python
fi

export PATH=${LCG_BASE}/Python-3.9.6:$PATH
export CPATH=${LCG_BASE}/include/python3.9:${CPATH}

if [[ $1 == "cmake" ]]; then
    install_cmake
fi

if [[ $1 == "boost" ]]; then
    install_boost
fi

cd ${BASE_DIR}
rm -rf ${DOWNLOADS}
