source /opt/rh/devtoolset-11/enable
export CC=/opt/rh/devtoolset-11/root/usr/bin/gcc
export CXX=/opt/rh/devtoolset-11/root/usr/bin/g++

source scl_source enable devtoolset-11

export PATH=/opt/rh/devtoolset-11/root/usr/bin:${PATH}

export BASE_DIR=$(dirname $(readlink -f ${BASH_SOURCE[0]:-${(%):-%x}}))

# LCG
export LCG_BASE=${BASE_DIR}/lcg
echo "LCG_BASE = ${LCG_BASE}"

# felix-external
export FELIX_EXTERNAL_DIR=${BASE_DIR}/felix-external
echo "FELIX_EXTERNAL_DIR = ${FELIX_EXTERNAL_DIR}"

export REGMAP_VERSION=0x0500
export CMAKE_BASE=${LCG_BASE}
export CMAKE_PATH=${CMAKE_BASE}/bin


export PATH=${LCG_BASE}/bin:${PATH}

export CPATH=${LCG_BASE}/include
export LD_LIBRARY_PATH=${LCG_BASE}/lib:${LD_LIBRARY_PATH}
export PKG_CONFIG_PATH=${LCG_BASE}/lib/pkgconfig:$PKG_CONFIG_PATH


export PATH=${LCG_BASE}/Python-3.9.6:$PATH
export CPATH=${LCG_BASE}/include/python3.9:${CPATH}

export CPATH=${FELIX_EXTERNAL_DIR}/include:${CPATH}
export LD_LIBRARY_PATH=${FELIX_EXTERNAL_DIR}/lib:${LD_LIBRARY_PATH}
export PKG_CONFIG_PATH=${FELIX_EXTERNAL_DIR}/lib/pkgconfig:$PKG_CONFIG_PATH
export LD_LIBRARY_PATH=${FELIX_EXTERNAL_DIR}/lib64:${LD_LIBRARY_PATH}
export PKG_CONFIG_PATH=${FELIX_EXTERNAL_DIR}/lib64/pkgconfig:$PKG_CONFIG_PATH

export TBB_ROOT=${FELIX_EXTERNAL_DIR}
