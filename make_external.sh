#!/bin/bash

# set environment variables
source ./setup_env.sh

echo "FELIX_EXTERNAL_DIR = ${FELIX_EXTERNAL_DIR}"
mkdir -p ${FELIX_EXTERNAL_DIR}/include
mkdir -p ${FELIX_EXTERNAL_DIR}/lib
mkdir -p ${FELIX_EXTERNAL_DIR}/lib64

mkdir build
cd build || exit
cmake ..
make -j
make
cd ..
rm -rf build
