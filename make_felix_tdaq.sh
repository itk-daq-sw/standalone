#!/bin/bash

source /opt/rh/devtoolset-11/enable
export CC=/opt/rh/devtoolset-11/root/usr/bin/gcc
export CXX=/opt/rh/devtoolset-11/root/usr/bin/g++

source scl_source enable devtoolset-11

export PATH=/opt/rh/devtoolset-11/root/usr/bin:${PATH}

#export TDAQ_BASE=$(dirname $(readlink -f ${BASH_SOURCE[0]:-${(%):-%x}}))/felix-sw
# export TDAQ_BASE="$PWD"/felix-sw
echo "TDAQ_BASE = ${TDAQ_BASE}"

git clone https://gitlab.cern.ch/atlas-tdaq-felix/software.git ${TDAQ_BASE}

cd ${TDAQ_BASE}

git clone https://gitlab.cern.ch/atlas-tdaq-felix/cmake_tdaq.git
cp -r ${BASE_DIR}/patch/* ${TDAQ_BASE}

git clone https://gitlab.cern.ch/atlas-tdaq-felix/python_env.git

export REGMAP_VERSION=0x0500

