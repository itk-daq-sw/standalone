include(ExternalProject)
include(FetchContent)

# TBB
function(add_external_tbb_2020)

    #message("tbb_2020: INSTALL_DIR = ${INSTALL_DIR}")

    if (TARGET tbb_2020)
        return()
    endif()

    # Architecture
    if ("${TARGET_ARCH}" STREQUAL "arm")
        set(TBB_ARCH "arch=arm")
    else()
        if ("${TARGET_ARCH}" STREQUAL "arm64")
            set(TBB_ARCH "arch=arm64")
        else()
            set(TBB_ARCH "")
        endif()
    endif()
    if ("${CMAKE_CXX_COMPILER_ID}" STREQUAL "Clang")
        set(TBB_COMP compiler=clang)
    else()
        set(TBB_COMP CXX=${CMAKE_CXX_COMPILER})
    endif()

    # Threading Building Blocks
    ExternalProject_Add (
        tbb_2020
        GIT_REPOSITORY https://github.com/oneapi-src/oneTBB.git
        GIT_TAG v2020.2

        BUILD_IN_SOURCE 1
        UPDATE_COMMAND ""
        CONFIGURE_COMMAND ""
        BUILD_COMMAND make -j ${TBB_COMP} ${TBB_ARCH} extra_inc=big_iron.inc tbb_build_prefix=lib
        INSTALL_COMMAND cp <SOURCE_DIR>/build/lib_release/libtbb.a <SOURCE_DIR>/build/lib_release/libtbbmalloc.a ${INSTALL_DIR}/lib
            COMMAND cp -r <SOURCE_DIR>/include/tbb <SOURCE_DIR>/include/serial ${HEADER_DIR}
    )
endfunction()


# libfabric
function(add_external_libfabric)

    if (TARGET libfabric)
       return()
    endif()

    set(enable_verbs "yes")
    if (NOT ENABLE_VERBS)
        set(enable_verbs "no")
    endif()

    # libfabric
    ExternalProject_Add (
        libfabric
        GIT_REPOSITORY https://github.com/ofiwg/libfabric.git
        GIT_TAG v1.16.1
        BUILD_IN_SOURCE 1
        CONFIGURE_COMMAND source ./autogen.sh && ./configure --prefix=${INSTALL_DIR} --enable-psm=no --enable-psm2=no --enable-psm3=no --enable-sockets=yes --enable-verbs=${enable_verbs} --enable-usnic=no --enable-gni=no --enable-udp=no --enable-tcp=no --enable-rxm=no --enable-rxd=no --enable-bgq=no --enable-shm=no --enable-mrail=yes --enable-rstream=no
        BUILD_COMMAND     make -j
        INSTALL_COMMAND   make install
    )
endfunction()


function(add_external_libsodium)
    ExternalProject_Add(libsodium
        # git giving error: not a git repository, using tar.gz instead
        DOWNLOAD_EXTRACT_TIMESTAMP ON
        URL https://download.libsodium.org/libsodium/releases/libsodium-1.0.18.tar.gz
        BUILD_IN_SOURCE 1
        CONFIGURE_COMMAND source ./autogen.sh && ./configure --prefix=${INSTALL_DIR}
        BUILD_COMMAND     make -j
        INSTALL_COMMAND   make install
    )
endfunction()


function(add_external_libzmq)
    ExternalProject_Add(libzmq
        DOWNLOAD_EXTRACT_TIMESTAMP ON
        URL https://github.com/zeromq/libzmq/archive/refs/tags/v4.3.0.zip
        BUILD_IN_SOURCE 1
        CONFIGURE_COMMAND source ./autogen.sh && ./configure --with-libsodium --prefix=${INSTALL_DIR}
        BUILD_COMMAND     make -j
        INSTALL_COMMAND   make install
        DEPENDS libsodium
    )
endfunction()


function(add_external_czmq)
    ExternalProject_Add(czmq
        DOWNLOAD_EXTRACT_TIMESTAMP ON
        URL https://github.com/zeromq/czmq/archive/refs/tags/v4.2.1.zip
        BUILD_IN_SOURCE 1
        CONFIGURE_COMMAND source ./autogen.sh && ./configure --prefix=${INSTALL_DIR}
        BUILD_COMMAND     make -j
        INSTALL_COMMAND   make install
        DEPENDS libzmq
    )
endfunction()


function(add_external_zyre)
    ExternalProject_Add(zyre
        DOWNLOAD_EXTRACT_TIMESTAMP ON
        URL https://github.com/zeromq/zyre/archive/refs/tags/v2.0.0.zip
        BUILD_IN_SOURCE 1
        CONFIGURE_COMMAND source ./autogen.sh && ./configure --prefix=${INSTALL_DIR}
        BUILD_COMMAND     make -j
        INSTALL_COMMAND   make install
        DEPENDS czmq
    )
endfunction()


function(add_external_libnuma)
    ExternalProject_Add(libnuma
        DOWNLOAD_EXTRACT_TIMESTAMP ON
        URL https://github.com/numactl/numactl/archive/refs/tags/v2.0.16.zip
        BUILD_IN_SOURCE 1
        CONFIGURE_COMMAND source ./autogen.sh && ./configure --prefix=${INSTALL_DIR}
        BUILD_COMMAND     make -j
        INSTALL_COMMAND   make install
    )
endfunction()


function(add_external_catch)
    ExternalProject_Add(catch
        GIT_REPOSITORY https://github.com/catchorg/Catch2.git
        GIT_TAG v2.13.10
        CONFIGURE_COMMAND ""
        BUILD_COMMAND ""
        INSTALL_COMMAND cp -r <SOURCE_DIR>//single_include/catch2/catch.hpp ${INSTALL_DIR}/include
    )
endfunction()


function(add_external_json)
    ExternalProject_Add(json
        GIT_REPOSITORY https://gitlab.cern.ch/atlas-tdaq-felix/external-json.git
        CONFIGURE_COMMAND ""
        BUILD_COMMAND ""
        INSTALL_COMMAND cp -r <SOURCE_DIR>/3.9.1/single_include/nlohmann ${INSTALL_DIR}/include
    )
endfunction()


function(add_external_jwrite)
    ExternalProject_Add(jwrite
        GIT_REPOSITORY https://gitlab.cern.ch/atlas-tdaq-felix/external-jwrite.git
        SOURCE_SUBDIR jwrite
        #SOURCE_DIR ${CMAKE_SOURCE_DIR}/external/jwrite
        CMAKE_ARGS -DCMAKE_INSTALL_PREFIX=${INSTALL_DIR}
    )
endfunction()


function(add_external_logrxi)
    ExternalProject_Add(logrxi
        GIT_REPOSITORY https://gitlab.cern.ch/atlas-tdaq-felix/external-logrxi.git
        SOURCE_SUBDIR rxi
        CMAKE_ARGS -DCMAKE_INSTALL_PREFIX=${INSTALL_DIR}
    )
endfunction()


function(add_external_simdjson)
    ExternalProject_Add(simdjson
        GIT_REPOSITORY https://gitlab.cern.ch/atlas-tdaq-felix/external-simdjson.git
        SOURCE_SUBDIR simdjson-1.0.0
        CMAKE_ARGS -DCMAKE_INSTALL_PREFIX=${INSTALL_DIR} -DCMAKE_CXX_FLAGS:STRING="-fPIC"
    )
endfunction()


function(add_external_docopt)
    ExternalProject_Add(docopt
        GIT_REPOSITORY https://github.com/docopt/docopt.cpp.git
        CMAKE_ARGS -DCMAKE_INSTALL_PREFIX=${INSTALL_DIR} -DCMAKE_CXX_FLAGS:STRING="-fPIC"
    )
endfunction()


function(add_external_yamlcpp)
    ExternalProject_Add(yaml-cpp
        GIT_REPOSITORY https://github.com/jbeder/yaml-cpp.git
        #GIT_TAG yaml-cpp-0.7.0
        CMAKE_ARGS -DCMAKE_INSTALL_PREFIX=${INSTALL_DIR}
    )
endfunction()


function(add_external_pybind11)
    ExternalProject_Add(pybind11
        GIT_REPOSITORY https://github.com/pybind/pybind11.git
        CMAKE_ARGS -DCMAKE_INSTALL_PREFIX=${INSTALL_DIR} -DPYBIND11_FINDPYTHON=ON
    )
endfunction()

function(add_external_spdlog)
    ExternalProject_Add(spdlog
        GIT_REPOSITORY  https://gitlab.cern.ch/atlas-sw-misc/spdlog.git
        GIT_TAG v1.9.2.x
        CMAKE_ARGS -DCMAKE_INSTALL_PREFIX=${INSTALL_DIR}
    )
endfunction()


function(add_external_bootstrap)
    ExternalProject_Add(bootstrap
        GIT_REPOSITORY https://github.com/twbs/bootstrap.git
        GIT_TAG v3.3.7
        CMAKE_ARGS -DCMAKE_INSTALL_PREFIX=${INSTALL_DIR}/include/bootstrap/3.3.7/
        #UPDATE_COMMAND ""
        CONFIGURE_COMMAND ""
        BUILD_COMMAND ""
        INSTALL_COMMAND mkdir -p ${HEADER_DIR}/bootstrap/3.3.7
            COMMAND cp -r <SOURCE_DIR>/dist/css   ${HEADER_DIR}/bootstrap/3.3.7
            COMMAND cp -r <SOURCE_DIR>/dist/fonts ${HEADER_DIR}/bootstrap/3.3.7
            COMMAND cp -r <SOURCE_DIR>/dist/js    ${HEADER_DIR}/bootstrap/3.3.7
    )
endfunction()


function(add_external_concurrentqueue)
    ExternalProject_Add(concurrentqueue
        GIT_REPOSITORY https://gitlab.cern.ch/atlas-tdaq-felix/external-concurrentqueue.git
        GIT_TAG 4.2.x
        SOURCE_SUBDIR 1.0.0.99
        CONFIGURE_COMMAND ""
        BUILD_COMMAND ""
        INSTALL_COMMAND cp -r <SOURCE_DIR>/1.0.0.99/include/concurrentqueue.h         ${HEADER_DIR}
                COMMAND cp -r <SOURCE_DIR>/1.0.0.99/include/blockingconcurrentqueue.h ${HEADER_DIR}
    )
endfunction()


function(add_external_simplewebserver)
    ExternalProject_Add(simplewebserver
        GIT_REPOSITORY https://gitlab.cern.ch/atlas-tdaq-felix/external-simplewebserver.git
        GIT_TAG simplewebserver-03-01-01
        SOURCE_SUBDIR 3.1.1
        CMAKE_ARGS -DCMAKE_INSTALL_PREFIX=${INSTALL_DIR}
    )
endfunction()


function(add_external_variant)
    ExternalProject_Add(variant
        GIT_REPOSITORY  https://gitlab.cern.ch/atlas-sw-misc/variant.git
        GIT_TAG v1.5.6
        CMAKE_ARGS -DCMAKE_INSTALL_PREFIX=${INSTALL_DIR}
        INSTALL_COMMAND cp -r <SOURCE_DIR>/include/rapidjson ${HEADER_DIR}
            COMMAND cp <SOURCE_DIR>/include/MemoryBuffer.hpp ${HEADER_DIR}
            COMMAND cp <SOURCE_DIR>/include/MsgPack.hpp      ${HEADER_DIR}
            COMMAND cp <SOURCE_DIR>/include/variant.hpp      ${HEADER_DIR}
        DEPENDS json
    )
endfunction()
