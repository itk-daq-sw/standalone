#!/bin/bash

# export BASE_DIR=$(dirname $(readlink -f ${BASH_SOURCE[0]:-${(%):-%x}}))
# export TDAQ_BASE=${BASE_DIR}/felix-sw

echo "BASE_DIR = ${BASE_DIR}"
echo "TDAQ_BASE = ${TDAQ_BASE}"

cd ${TDAQ_BASE}

# source ${BASE_DIR}/setup_env.sh
# source ${TDAQ_BASE}/setup.sh

source ./git_clone_all.sh

cmake_config

cd x86_64-centos7-gcc11-opt

make -j

. ../install.sh

# make install

# cp -r ../felix-bus-fs/felixbus ../installed/include
# cp -r ../netio-next/netio      ../installed/include

# mkdir -p ../installed/include/felix

# cp ../felix-interface/felix/felix_client_thread_interface.hpp  ../installed/include/felix/
# cp ../felix-interface/felix/felix_client_thread_extension.hpp  ../installed/include/felix/

# cp ../felix-client/felix/felix_client_context_handler.hpp      ../installed/include/felix/
# cp ../felix-client/felix/felix_client_util.hpp                 ../installed/include/felix/
# cp ../felix-client/felix/felix_client_info.hpp                 ../installed/include/felix/

# cp ../felix-def/include/felix/felix_toflx.hpp                  ../installed/include/felix/

# # lib
# cp felix-client/libfelix-client-lib*  ../installed/lib/

# make clean
