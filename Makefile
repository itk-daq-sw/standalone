	
build:
	docker build -f Dockerfile-lcg -t itk-daq-sw-lcg --progress plain .
	docker build -f Dockerfile-felix-external -t itk-daq-sw-felix-external --progress plain .
	docker build -f Dockerfile-felix -t itk-daq-sw-felix --progress plain .

push:
	docker login gitlab-registry.cern.ch	
	docker tag itk-daq-sw-felix gitlab-registry.cern.ch/itk-daq-sw/standalone/itk-daq-sw-felix
	docker push gitlab-registry.cern.ch/itk-daq-sw/standalone/itk-daq-sw-felix