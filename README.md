# Standalone base software distribution

## Introduction

At the moment, the FELIX software https://gitlab.cern.ch/atlas-tdaq-felix/software distribution can be compiled
only if one has `LCG` and access to `cvmfs`. The 'official' solution is to locally dowload `cvmfs` image.
The most of the FELIX external packages are precompiled and used as `*.so` files. In general, this is not a correct approach.

Standalone base software distribution downloads and compiles all packages required for `atlas-felix-tdaq` package
and does not require access to `cvmfs`. All packages are downloaded online from corresponding repositories and compiled from sources.

The `lcg` folder contains installed `Python-3.9.6`, `CMake-3.25.1`, `Boost-1.77.0` packages.

All external packages for FELIX distribution are installed in `felix-external` folder.

### Docker containers
Solution with docker containers for `felixcore` and `felix-star`
can be found here: [felix-container](https://gitlab.cern.ch/atlas-itk-pixel-systemtest/itk-demo-sw/containers/felix-container).

### FELIX documentation
More information about FELIX distribution documentation https://gitlab.cern.ch/atlas-tdaq-felix/felix-distribution.


## How to build standalone distribution

### Install devtoolset-11
```
sudo yum install devtoolset-11
source scl_source enable devtoolset-11
```

### Clone this repository
```
git clone https://gitlab.cern.ch/itk-daq-sw/standalone.git
cd standalone
```

### Install `LCG`
```
source make_lcg.sh
```
> This script downloads and installs `Python-3.9.6`, `CMake-3.25.1` and `Boost-1.77.0` packages.

### Install `felix-external`
```
source setup_env.sh
mkdir build
cd build
cmake ..
make -j
```
> This script compiles and install all external packages required for FELIX distribution in `felix-external` folder.
All header files are installed in common `include` folder, all libraries in common `lib` and `lib64` folders.
This differs from an official approach, where each external package has it's own folder for header files and libraries.
The `build` folder can be deleted after installation.

### Install FELIX distribution
The FELIX dustribution `felix-sw` can be installed in any location. Corresponding path have to be exported in `TDAQ_BASE` environmental variable.
```
git clone https://gitlab.cern.ch/atlas-tdaq-felix/software.git felix-sw
export TDAQ_BASE=<path-to-felix-sw>
cd felix-sw
```
> Clone `cmake_tdaq` package.
```
git clone https://gitlab.cern.ch/atlas-tdaq-felix/cmake_tdaq.git
```
> To reflect standalone behaviour, the patch have to be applied for both packages: `felix-sw` (replace `CMakeLists.txt`) and `cmake_tdaq`
(patch module files `FELIX.cmake`, `TDAQ.cmake` and `cmake/setup.sh`).
```
cp -r ${BASE_DIR}/standalone/patch/* ${TDAQ_BASE}
```
> Clone python environment.
```
git clone https://gitlab.cern.ch/atlas-tdaq-felix/python_env.git
```

### Setup environment variables.
```
source <path-to-standalone>/setup_env.sh
source <path-to-felix-sw>/setup.sh
```
> Next step is to clone packages. One can clone all packages by running
```
source git_clone_all.sh
```
this script clones all FELIX packages (without cloning felix external packages).
For less functionality one can clone only necessary packages (see lists below).

### Compile and install FELIX software
```
cmake_config
cd x86_64-centos7-gcc11-opt
make -j
source ../install.sh
cd ..
```
> `source.sh` script installs all packages and copies additional include files and `felix-client-lib` library required for felix-client.
## Independently compilable packages

Some libraries can be compiled without cloning all packages.

### netio and netio-next
For netio and netio-next communication the following packages are required:
```
git clone https://gitlab.cern.ch/atlas-tdaq-felix/felix-tag.git
git clone https://gitlab.cern.ch/atlas-tdaq-felix/netio.git
git clone https://gitlab.cern.ch/atlas-tdaq-felix/netio-next.git
```

### felix-client
To compile felix-client library, one have to add:
```
git clone https://gitlab.cern.ch/atlas-tdaq-felix/felix-def.git
git clone https://gitlab.cern.ch/atlas-tdaq-felix/felix-interface.git
git clone https://gitlab.cern.ch/atlas-tdaq-felix/felix-client-thread.git
git clone https://gitlab.cern.ch/atlas-tdaq-felix/felix-unit-test.git
git clone https://gitlab.cern.ch/atlas-tdaq-felix/felix-bus-fs.git
git clone https://gitlab.cern.ch/atlas-tdaq-felix/hdlc_coder.git
git clone https://gitlab.cern.ch/atlas-tdaq-felix/felix-client.git
```

### felix-star
The felix-star can be compiled by adding:
```
git clone https://gitlab.cern.ch/atlas-tdaq-felix/flxcard.git
git clone https://gitlab.cern.ch/atlas-tdaq-felix/flxcard_py.git
git clone https://gitlab.cern.ch/atlas-tdaq-felix/drivers_rcc.git
git clone https://gitlab.cern.ch/atlas-tdaq-felix/regmap.git
git clone https://gitlab.cern.ch/atlas-tdaq-felix/felix-star.git
```

### felixcore
Needed for felixcore support. Will be removed for regmap 5.0.
```
git clone https://gitlab.cern.ch/atlas-tdaq-felix/packetformat.git
git clone https://gitlab.cern.ch/atlas-tdaq-felix/felixbus.git
git clone https://gitlab.cern.ch/atlas-tdaq-felix/felixbase.git
git clone https://gitlab.cern.ch/atlas-tdaq-felix/felixcore.git
```

### others
```
git clone https://gitlab.cern.ch/atlas-tdaq-felix/felig-tools.git
git clone https://gitlab.cern.ch/atlas-tdaq-felix/ftools.git
git clone https://gitlab.cern.ch/atlas-tdaq-felix/felix-mapper.git
git clone https://gitlab.cern.ch/atlas-tdaq-felix/felix-fpga-flash.git
git clone https://gitlab.cern.ch/atlas-tdaq-felix/firmware_loader.git
git clone https://gitlab.cern.ch/atlas-tdaq-felix/data_transfer_tools.git
git clone https://gitlab.cern.ch/atlas-tdaq-felix/wuppercodegen.git
```
